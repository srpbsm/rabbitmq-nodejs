const onMessage = function (message) {
  document.querySelector('#messages').innerHTML += `<li><strong>${message.message}</strong> <i>${message.timestamp}</i> &nbsp;&nbsp;<b>${message.priority}</b></li>`
}

const connectToServer = function () {
  const socket = io('/')
  socket.on('message', onMessage)
}

connectToServer()
