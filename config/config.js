module.exports = {
  port: process.env.PORT || 3000,
  rabbit: {
    connectionString: 'amqp://localhost:5672',
    queue: 'codingtest',
  },
}
