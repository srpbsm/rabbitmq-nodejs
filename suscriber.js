const amqp = require('amqplib')
const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const config = require('./config/config')
const createDirIfNotExits = require('./utils/createDirIfNotExits')
const errorHandler = require('./utils/errorHandler')

// create dir if not present
// and dynamically create file
// and append consume message from rabbitmq to a json file **2
const dirname = 'data'
const filename = new Date()
createDirIfNotExits(dirname)
const createFile = require('./utils/writeToFile')
const appendContentToFile = createFile(`${dirname}/${filename.toString()}`)

app.use(express.static(__dirname + '/public'))

server.listen(config.port, () => {
  console.log(`listening on *:${config.port}`)
})
;(async () => {
  try {
    const connection = await amqp.connect(config.rabbit.connectionString)
    const channel = await connection.createChannel()
    const QUEUE = config.rabbit.queue
    channel.assertQueue(QUEUE)

    io.on('connection', (socket) => {
      channel.consume(QUEUE, (message) => {
        const input = JSON.parse(message.content.toString())

        // append consume message from rabbitmq to a json file **2
        // dump to cache or db here i have dump to a file
        appendContentToFile(input)

        // subscriber will filter for every priority >= 7 messages and push those
        // messages into Socket.IO for the front end to display
        if (input.priority >= 7) socket.emit('message', input)

        // removes from rabbitmq queue
        channel.ack(message)
      })

      console.log('a user connected')
    })
  } catch (err) {
    errorHandler(err)
    // ** handler error ** //
  }
})()
