const amqp = require('amqplib')
const config = require('../config/config')

const channelConnection = async (queue, durable = false) => {
  try {
    const connection = await amqp.connect(config.rabbit.connectionString)
    const channel = await connection.createChannel()
    console.log('channel ', channel)
    await channel.assertQueue(queue, (durable = false))
    return channel
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}
console.log('ass', channelConnection())
module.exports = channelConnection()
