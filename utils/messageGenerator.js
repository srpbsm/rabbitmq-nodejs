const txtgen = require('txtgen')

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const messageGenerate = () => {
  return {
    message: txtgen.sentence(),
    timestamp: new Date(),
    priority: randomNumber(1, 10),
  }
}

module.exports = messageGenerate
