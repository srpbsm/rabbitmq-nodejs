const fs = require('fs')

function filename(filename) {
  // 'a' flag stands for 'append'
  const log = fs.createWriteStream(filename, { flags: 'a' })
  return function write(params) {
    log.write(JSON.stringify(params) + '\n')
  }
}

module.exports = filename

// you can skip closing the stream if you want it to be opened while
// a program runs, then file handle will be closed
// log.end()
