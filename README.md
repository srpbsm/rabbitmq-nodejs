# RabbitMQ Socket-IO Nodejs

simulating a high-volume data input environment using the following technology stack

1. A message queue (RabbitMQ)
2. A NodeJS backend to push information into the queue (publish)
3. A NodeJS backend to read from the queue (subscribe) and to ​ filter out ​ certain messages to be sent to the front end via Socket.IO
4. receive the messages via Socket.IO and display using plain html
5. The publisher will publish 20 messages per second.
6. The NodeJS subscriber will filter for every priority >= 7 messages and push those messages into Socket.IO for the front end to display

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install

```
Docker
NodeJS
```

### Installing

Before Installing, Setup RabbitMQ ( install rabbitmq ) if you have docker install on computer please run below command other wise please install rabbitmq leave default port to 5672

```
docker run -d --name rabbitmq -p 5672:5672 rabbitmq

```

Clone repo and cd to the project repo then

```
npm install
npm run start

```

For Frontend display

[http://localhost:3000](http://localhost:3000)

All data cosumed by consumer are stored in a file in a data directory. Only priority >= 7 message are push into Socket.IO for the front end to display.
