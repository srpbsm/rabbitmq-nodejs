const amqp = require('amqplib')
const messageGenerator = require('./utils/messageGenerator')
const config = require('./config/config')
const errorHandler = require('./utils/errorHandler')

;(async () => {
  try {
    const connection = await amqp.connect(config.rabbit.connectionString)
    const channel = await connection.createChannel()
    const QUEUE = config.rabbit.queue
    channel.assertQueue(QUEUE)
    let count = 0

    // publisher will publish 20 messages per second
    setInterval(function () {
      count++
      const message = messageGenerator()
      channel.sendToQueue(QUEUE, Buffer.from(JSON.stringify(message)))
      // console.log(message)
      console.log(count)
    }, 50)

    return true
  } catch (err) {
    errorHandler(err)
  }
})()
